﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    const int maxHealth = 5;
    const float heartSpacing = 50;
    [SerializeField] int currentHealth = 3;
    [SerializeField] RectTransform itemsBackGround;
    [SerializeField] GameObject heartImage;

    private void Start()
    {
        DrawHearts();
    }

    public void DecreseHealth(int amount)
    {
        currentHealth -= amount;
        DrawHearts();
        Debug.Log(gameObject.name + " took " + amount + " damage.");
    }

    private void DrawHearts()
    {
        Vector2 heartSize = heartImage.GetComponent<RectTransform>().rect.size;
        float heartSpacing = heartSize.x + 20f;
        for (int i = 1; i <= currentHealth; i++)
        {
            var item = Instantiate(heartImage,
                       new Vector2(itemsBackGround.anchoredPosition.x + (heartSpacing * i),
                       itemsBackGround.anchoredPosition.y)
                       ,Quaternion.identity);

            item.transform.SetParent(itemsBackGround.transform, false);
        }
    }

    public void IncreaseHealth(int amount)
    {

    }
}
