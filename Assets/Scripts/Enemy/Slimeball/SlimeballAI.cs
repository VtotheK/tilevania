﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SlimeballAI : MonoBehaviour
{
    [SerializeField] GameObject player;
    private bool onIdle = true;

    SlimeballAnimationController animationController;
    // Start is called before the first frame update
    void Start()
    {
        animationController = GetComponent<SlimeballAnimationController>();
    }

    private void Update()
    {
        CheckPlayerPosition();
    }

    private void CheckPlayerPosition()
    {
        float distance = Mathf.Abs(gameObject.transform.position.x - player.transform.position.x);
       if(distance <= 5 && onIdle)
        {
            int playerPos = transform.position.x > player.transform.position.x ? -1 : 1;
            gameObject.transform.localScale = new Vector3 (playerPos,1,1);
            string changeBool = animationController.isMoving;
            animationController.ChangeAnimationBoolParameter(changeBool, true);
            onIdle = false;
        }
        else
        {
            return;
        }
    }
}
