﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class SlimeballAnimationController : MonoBehaviour
{
    const string ISMOVING_BOOL = "isMoving";

    float xVelocity = 5f;
    float yVelocity = 5f;
    Animator animator;

    public string isMoving
    {
        get { return ISMOVING_BOOL; }
    }

    void Start()
    {
        animator = GetComponent<Animator>();
        animator.SetBool(ISMOVING_BOOL, false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ChangeAnimationBoolParameter(string boolName, bool value)
    {
        animator.SetBool(boolName, value);
    }
}
