﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(SlimeballAnimationController))]

public class SlimeballMovement : MonoBehaviour
{
    public delegate void MoveEnemy();
    public static event MoveEnemy moveEnemy;

    [SerializeField] float gravity = 8f;
    [SerializeField] LayerMask rayLayerMask;

    Rigidbody2D rigidBody;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
    }

    public void Move()
    {
        rigidBody.velocity = new Vector3(Mathf.Sign(transform.localScale.x) * 3, 3, 0);
    }

    public void CheckRay()
    {
        float xDirection = Mathf.Sign(transform.localScale.x);
        Vector2 rayOrigin = new Vector2(transform.position.x + (xDirection * 2.3f), transform.position.y);

        RaycastHit2D[] allHits = Physics2D.RaycastAll(rayOrigin, Vector2.down, 1f, rayLayerMask);

        foreach (RaycastHit2D hit in allHits)
        {
            if (hit.collider.gameObject.layer == 9)
            {
                return;
            }
        }
        transform.localScale = new Vector3(-xDirection, 1, 1);
    }
}
