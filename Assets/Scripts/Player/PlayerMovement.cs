﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(PlayerAnimationController))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Controller2D))]


public class PlayerMovement : MonoBehaviour {
    public enum PushDirection { Left = -1, Up = 0, Right = 1};
    [Header("Character movement")]
    [SerializeField] float timeToJumpApex = 0.4f;
    [SerializeField] float jumpHeight;
    [SerializeField] float accelerationTimeGrounded;
    [SerializeField] float accelerationTimeAirborne;
    [SerializeField] float runSpeed = 8f;
    [SerializeField] float walkSpeed = 5f;
    [SerializeField] float climbSpeed = 5f;
    [Header("Other")]
    [SerializeField] LayerMask ladderMask;
    [SerializeField] GameObject lavaDeathParticles;

    GameObject _lavaDeathParticles;

    #region Directly connected to movement
    float gotHitTimer;
    float moveSpeed;
    float xMoveSmoothing;
    float gravity;
    float jumpVelocity;

    #endregion
    #region Jumping, climbing and orientation
    bool jumping = false;
    bool climbing = false;
    float jumpStartTime = 0;
    bool facingRight = true;
    #endregion
    #region Other stuff
    float? diePosY = null;
    bool gotHit = false;
    bool lavaHazardDeath = false;
    bool alive = true;
    #endregion
    #region Cached references
    BoxCollider2D collider;
    PlayerAnimationController playerAnimationController;
    Controller2D controller;
    #endregion

    PushDirection pushDirection;
    Vector3 velocity;


    public delegate void changeAnimation(string animStringReference, bool parameter);
    public static event changeAnimation animatorEvent;

    #region Properties
    public bool LavaHazardDeath
    {
        get { return lavaHazardDeath; }
        set { lavaHazardDeath = value; }
    }
    public bool Alive
    {
        get { return alive; }
        set { alive = value; }
    }
    #endregion

    private void Start()
    {
        alive = true;
        gotHit = false ;
        collider = GetComponent<BoxCollider2D>();
        playerAnimationController = GetComponent<PlayerAnimationController>();
        controller = GetComponent<Controller2D>();
        gravity = -Mathf.Abs((2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2));
        jumpVelocity = Mathf.Abs(gravity * timeToJumpApex);
        moveSpeed = walkSpeed;
    }

    private void Update()
    {
       OnHit();
       if (controller.collisionInfo.collisionAbove || controller.collisionInfo.collisionBelow)
        {
          velocity.y = 0;
        }
        if (alive)
        {
            Jump();
            Vector2 input = HorizontalMovement();
            velocity.y += gravity * Time.deltaTime;
            Climb();
            float targetVelocityX = input.x * moveSpeed;
            velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref xMoveSmoothing, GetSmoothTime());
            WallCling();
            controller.Move(velocity * Time.deltaTime);
        }
        else if(!alive && lavaHazardDeath)
        {
            LavaDeathMovement();  
        }
    }

    private void LavaDeathMovement()
    {
        float height = collider.size.y / 1.5f;

        if (diePosY == null)
        {
           diePosY = GetComponent<Transform>().position.y;
           _lavaDeathParticles = Instantiate(lavaDeathParticles, gameObject.transform.position, Quaternion.Euler(-90, 0, 0)) as GameObject;
            _lavaDeathParticles.transform.SetParent(gameObject.transform);
            _lavaDeathParticles.transform.localScale = new Vector3(1, 1, 1);
        }

        if (diePosY - transform.position.y < height)
        {
            Vector3 deathMovement = new Vector3(0, -0.2f * Time.deltaTime, 0);
            transform.Translate(deathMovement);
            //_lavaDeathParticles.transform.Translate(new Vector3(0,0,-0.2f * Time.deltaTime));
        }
        else if(diePosY - transform.position.y >= height)
        {
            if (_lavaDeathParticles != null)
            {
                _lavaDeathParticles.GetComponent<ParticleSystem>().Stop();
            }
        }
    }

    private void WallCling()
    {
        if((controller.collisionInfo.collisionRight || controller.collisionInfo.collisionLeft ) && 
            !controller.collisionInfo.collisionBelow && (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow)))
        {
            if(velocity.y < 0 )
            {
                velocity.y = velocity.y / 2;
            }
        }
    }

    private void OnHit()
    {
        if (gotHit)
        {
            int direction = (int)pushDirection;
            if (direction != 0)
            {
                velocity = new Vector3(0.35f * direction, 0.1f, 0);
            }
            else
            {
                velocity = new Vector3(0.35f * direction, 0.5f, 0);
            }
            controller.Move(velocity);
            if (Time.time - gotHitTimer >= 0.09f)
            {
                gotHit = false;
            }
        }
    }

    private void Climb()
    {
        bool onLadder = collider.IsTouchingLayers(ladderMask) ? true : false;
        if (!onLadder)
        {
            if(animatorEvent != null)
            {
                playerAnimationController.SetAnimatorSpeed(1);
                animatorEvent(PlayerAnimationController.ANIMATOR_CLIMB_BOOL, false);
            }
            climbing = false;
            return;
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            climbing = true;
            if (animatorEvent != null)
            {
                if (animatorEvent != null)
                {
                    animatorEvent(PlayerAnimationController.ANIMATOR_WALK_BOOL, false);
                    animatorEvent(PlayerAnimationController.ANIMATOR_CLIMB_BOOL, true);
                }
                playerAnimationController.SetAnimatorSpeed(1);
            }
            velocity.x = 0;
            velocity.y = climbSpeed;
        }
        else if(climbing)
        {
            playerAnimationController.SetAnimatorSpeed(0);
            velocity.y = 0;
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            velocity.y = -climbSpeed;
            if(animatorEvent != null) { 
            animatorEvent(PlayerAnimationController.ANIMATOR_CLIMB_BOOL, true);
                }
            playerAnimationController.SetAnimatorSpeed(1);
        }
    }

    private void Jump()
    {

        if (Input.GetKey(KeyCode.Space))
        {
            if (jumpStartTime == 0)
            {
                jumping = true;
                jumpStartTime = Time.time;
            }
            if (Time.time - jumpStartTime <= 0.200000f && jumping)
            {
                velocity.y = jumpVelocity;
            }
        }

        else if (controller.collisionInfo.collisionBelow || Time.time - jumpStartTime <= 0.2f)
        {
            accelerationTimeAirborne = 0.13f;
            jumping = false;
            jumpStartTime = 0;
        }
        else if (!controller.collisionInfo.collisionBelow)
        {
            jumpStartTime = -1;
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            jumping = false;
        }

        if ((controller.collisionInfo.collisionLeft || controller.collisionInfo.collisionRight) && !jumping && jumpStartTime == -1 && !controller.collisionInfo.collisionBelow)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                float xDirection = Mathf.Sign(velocity.x);
                accelerationTimeAirborne = 0.45f;
                velocity = new Vector2(-xDirection * 15, 15); //WallJump
            }
        }
    }

    private Vector2 HorizontalMovement()
    {
        float inputX;
        float inputY;
        if (Input.GetKey(KeyCode.RightArrow) && Input.GetKey(KeyCode.LeftArrow))
        {
            animatorEvent(PlayerAnimationController.ANIMATOR_WALK_BOOL, false);
            return Vector2.zero;
        }
        if (Input.GetKeyDown(KeyCode.LeftShift) && !climbing)
        {
            playerAnimationController.SetAnimatorSpeed(1);
            moveSpeed = runSpeed;
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift) && !climbing)
        {
            playerAnimationController.SetAnimatorSpeed(0.5f);
            moveSpeed = walkSpeed;
        }
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))
        {
            inputX = Input.GetAxisRaw("Horizontal");
            inputY = Input.GetAxisRaw("Vertical");
            if (animatorEvent != null && !climbing)
            {
                animatorEvent(PlayerAnimationController.ANIMATOR_WALK_BOOL, true);
            }
            GetComponent<Transform>().localScale = inputX == -1 ? new Vector3(-1, 1, 1) : new Vector3(1, 1, 1);
        }
        else
        {
            if (animatorEvent != null)
            {
                animatorEvent(PlayerAnimationController.ANIMATOR_WALK_BOOL, false);
            }

            inputX = 0;
            inputY = 0;
        }
        Vector2 input = new Vector2(inputX, inputY);
        return input;
    }

    private float GetSmoothTime()
    {
        return controller.collisionInfo.collisionBelow == true ? accelerationTimeGrounded : accelerationTimeAirborne;
    }

    public void PushPlayer(PushDirection direction)
    {
        pushDirection = direction;
        gotHitTimer = Time.time;
        gotHit = true;
    }
}

