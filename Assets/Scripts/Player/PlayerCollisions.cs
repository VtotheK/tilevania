﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(PlayerHealth))]
public class PlayerCollisions : MonoBehaviour
{


    PlayerMovement playerMovement;
    private void Start()
    {
        playerMovement = GetComponent<PlayerMovement>();
        PlayerHealth.deathEvent += IsStandingOnHazard;
    }

    [SerializeField] LayerMask layerMask; //Check collision with dynamic rigidbodies f.e enemies and so on
    private void OnCollisionEnter2D(Collision2D collision)
    {
        var otherColliderType= collision.collider.GetType();
        var playerColliderType = collision.otherCollider.GetType();
        PlayerHealth playerHealth = GetComponent<PlayerHealth>();
        if (CheckDynamicLayers(collision) && playerColliderType == typeof(BoxCollider2D))
        {                                                   
            if (collision.gameObject.GetComponent<DamageDealer>())
            {
                int damage = collision.gameObject.GetComponent<DamageDealer>().BaseDamage;
                playerHealth.DecreseHealth(damage);
            }
            int pushDirection = collision.transform.position.x > transform.position.x ? -1 : 1;
            if (pushDirection == 1)
            {

                GetComponent<PlayerMovement>().PushPlayer(PlayerMovement.PushDirection.Right);
            }
            else if (pushDirection == -1)
            {
                GetComponent<PlayerMovement>().PushPlayer(PlayerMovement.PushDirection.Left);
            }

        }
        else if (CheckDynamicLayers(collision) && otherColliderType == typeof(CapsuleCollider2D)
            && playerColliderType == typeof(CapsuleCollider2D)) //TODO same here as above
        {
            GetComponent<PlayerMovement>().PushPlayer(PlayerMovement.PushDirection.Up);
        }
    }

    private bool CheckDynamicLayers(Collision2D collision)
    {
        return collision.otherCollider.IsTouchingLayers(layerMask);
    }

    public void IsStandingOnHazard()
    {
        GetComponentInChildren<SpriteRenderer>().color = new Color(200, 0, 0, 255);
        playerMovement.LavaHazardDeath = true;
    }

    private void OnDisable()
    {
        PlayerHealth.deathEvent -= IsStandingOnHazard;
    } // Remove function from onDeath delegate.
}
