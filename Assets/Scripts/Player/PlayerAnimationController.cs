﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationController : MonoBehaviour
{
    public const string ANIMATOR_IS_ALIVE_BOOL = "IsAlive";
    public const string ANIMATOR_CLIMB_BOOL = "IsClimbing";
    public const string ANIMATOR_WALK_BOOL = "IsWalking";

    PlayerMovement playerMovement;
    Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        playerMovement = GetComponent<PlayerMovement>();
    }
    void Start()
    {
        PlayerMovement.animatorEvent += SetAnimationBool;
        PlayerHealth.animationEvent += SetAnimationBool;
    }

    public void SetAnimatorSpeed(float speed)
    {
        animator.speed = speed;
    }

    private void SetAnimationBool(string sReference, bool parameter)
    {
        animator.SetBool(sReference, parameter);
    }

    private void OnDisable()
    {
        PlayerMovement.animatorEvent -= SetAnimationBool;
        PlayerHealth.animationEvent -= SetAnimationBool;
    }
}
