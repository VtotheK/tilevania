﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    const int maxHealth = 5;
    const float heartSpacing = 50;
    [SerializeField] int currentHealth = 3;
    [SerializeField] RectTransform itemsBackGround;
    [SerializeField] GameObject heartImage;


    public delegate void changeAnimation(string sReference, bool parameter);
    public static event changeAnimation animationEvent;

    public delegate void onDeath();
    public static event onDeath deathEvent;

    private void Start()
    {
        DrawHearts();
    }

    public void DecreseHealth(int amount)
    {
        currentHealth -= amount;
        if(currentHealth <= 0)
        {
            DrawHearts();
            Die();
            return;
        }
        DrawHearts();
        Debug.Log(gameObject.name + " took " + amount + " damage.");
    }

    private void Die()
    {
        if(animationEvent != null)
        {
            animationEvent(PlayerAnimationController.ANIMATOR_IS_ALIVE_BOOL, false);
        }
        if(deathEvent != null)
        {
            deathEvent();
        }
        GetComponent<PlayerMovement>().Alive = false;
        
    }

    private void DrawHearts()
    {
        int heartAmount = itemsBackGround.childCount;
        for (int i = 0; i < heartAmount; i++)
        {
            Destroy(itemsBackGround.GetChild(i).gameObject);
        }
        Vector2 heartSize = heartImage.GetComponent<RectTransform>().rect.size;
        float heartSpacing = heartSize.x + 20f;
        for (int i = 1; i <= currentHealth; i++)
        {
            var item = Instantiate(heartImage,
                       new Vector2(itemsBackGround.anchoredPosition.x + (heartSpacing * i),
                       itemsBackGround.anchoredPosition.y)
                       , Quaternion.identity);

            item.transform.SetParent(itemsBackGround.transform, false);
        }
    }

    public void IncreaseHealth(int amount)
    {
        currentHealth += amount;
        DrawHearts();
    }
}
