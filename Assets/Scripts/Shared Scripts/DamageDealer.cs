﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageDealer : MonoBehaviour
{
    [SerializeField] int baseDamage = 10;
    [SerializeField] int stompDamage = 5;

    public int BaseDamage
    {
        get { return baseDamage; }
    }
    public int StompDamage
    {
        get { return baseDamage; }
    }
}
