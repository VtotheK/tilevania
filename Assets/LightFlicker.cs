﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlicker : MonoBehaviour
{
    [SerializeField] float minFlickTime = 0.02f;
    [SerializeField] float maxFlickTime = 0.4f;
    [SerializeField] float minIntensity = 4f;
    [SerializeField] float maxIntensity = 10f;

    private float currentFlick;
    private float lastFlick = 0f;

    Light light;

    private void Start()
    {
        light = GetComponent<Light>();
    }

    private void FixedUpdate()
    {
        Flick();
    }

    private void Flick()
    {
       if(Time.time - lastFlick > currentFlick)
        {
            currentFlick = UnityEngine.Random.Range(minFlickTime, maxFlickTime);
            lastFlick = Time.time;
            light.intensity = UnityEngine.Random.Range(minIntensity, maxIntensity);
        }
    }
}
