﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hazard : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var incomingCollision = collision.GetType();
        if(incomingCollision == typeof(BoxCollider2D) && collision.GetComponent<PlayerHealth>())
        {
            collision.GetComponent<PlayerHealth>().DecreseHealth(10);
        }
    }
}
